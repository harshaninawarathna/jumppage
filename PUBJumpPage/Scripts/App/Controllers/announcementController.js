﻿announcementModule.controller('announcementController', function ($scope, announcementRepository) {
    $scope.announcements = [];

    $scope.loadAnnouncements = function () {
        announcementRepository.get().then(function (list) {
            $scope.announcements = list;
        });
    };
});