﻿announcementModule.controller('galleryController', function ($scope, galleryRepository) {
    $scope.timeLineItems = [];
    $scope.selectedImagePath;
    $scope.years = [];

    $scope.loadGallery = function () {
        galleryRepository.get().then(function (list) {
            $scope.timeLineItems = list;
            $scope.selectedImagePath = list[0].ImagePath;

            for (var i = 0; i < list.length; i++) {
                $scope.years.push(list[i].Year);
            }

            $scope.buildSlider = function () {
                var slider = document.getElementById('skipstep');

                var min = $scope.years[0];
                var noOfYears = $scope.years.length;
                var max = $scope.years[noOfYears - 1];

                noUiSlider.create(slider, {
                    start: 2015,
                    range: { 'min': min, 'max': max },
                    pips: { // Show a scale with the slider
                        mode: 'steps'
                    }
                });
            }

        });

       
    };

    $scope.onButtonClick = function (imagePath) {
        $scope.selectedImagePath = imagePath;
    };

    
});