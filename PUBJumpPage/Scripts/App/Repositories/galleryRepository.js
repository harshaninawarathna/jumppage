﻿announcementModule.factory('galleryRepository', function ($http, $q) {
    return {
        get: function () {
            var deffered = $q.defer();
            $http.get('/api/Gallery').success(deffered.resolve).error(deffered.reject);
            return deffered.promise;
        }
    };
});