﻿var announcementModule = angular.module('announcement', ['ngRoute']);

announcementModule.config(['$routeProvider', '$locationProvider', function ($routeProvider, $locationProvider) {
    $routeProvider
        .when('/announcement', {
            templateUrl: '/Templates/Announcement/Announcement.html',
            controller: 'announcementController'
        })
        .when('/gallery', {
            templateUrl: '/Templates/Gallery/Gallery.html',
            controller: 'galleryController'
        })
        .otherwise({ redirectTo: '/gallery' });
}]);