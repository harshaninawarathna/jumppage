﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PUBJumpPage.Models
{
    public class Announcement
    {
        public string Message { get; set; }

        public string UpdatedTime { get; set; }
    }
}