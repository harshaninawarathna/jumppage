﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PUBJumpPage.Models
{
    public class TimeLineItem
    {
        public string Year { get; set; }
        public string ImagePath { get; set; }
    }
}