﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using DHI.Components;

namespace PUBJumpPage.Utility
{
    public class Utility
    {
        public static string GetFileLocation(string path)
        {
            return Path.Combine(AppDomain.CurrentDomain.BaseDirectory, path);
        }

        public static Config GetConfig()
        {
            var config = new Config(Utility.GetFileLocation(@"Settings\Settings.json"));
            return config;
        }
    }
}