﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using PUBJumpPage.Models;
using System.IO;

namespace PUBJumpPage.Controllers
{
    public class GalleryController : ApiController
    {
        // GET /api/Gallery
        public List<TimeLineItem> GetGallery()
        {
            var config = Utility.Utility.GetConfig();
            var timeline = config.val.timeline;
            var items = new List<TimeLineItem>();

            foreach (var item in timeline)
            {
                var timeLineItem = new TimeLineItem();
                timeLineItem.Year = (string)item.year;
                timeLineItem.ImagePath = (string)item.imagePath;
                items.Add(timeLineItem);
            }           
            return items;
        }
    }
}
