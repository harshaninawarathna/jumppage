﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using PUBJumpPage.Models;
using DHI.Components;
using PUBJumpPage.Utility;
using System.IO;

namespace PUBJumpPage.Controllers
{
    public class AnnouncementController : ApiController
    {
        // GET api/Announcement
        public List<Announcement> GetAnnouncements()
        {
            var announcements = new List<Announcement>();
            var dirPath = GetAnnouncementDirectoryPath();
            var filePaths = GetAnnouncementFilePathsFromDirectory(dirPath);

            for (var i = 0; i < filePaths.Length; i++ )
            {
                var announcement = new Announcement();
                var filePath = filePaths[i];
                var filePathComponents = filePath.Split('\\');
                var fileName = filePathComponents[filePathComponents.Length - 1];

                announcement.UpdatedTime = fileName.Split('_')[0];
                announcement.Message = File.ReadAllText(filePath);
                announcements.Add(announcement);
            }

            //order latest announcement first
            announcements = announcements.OrderByDescending(a => a.UpdatedTime).ToList();
            return announcements;
        }

        private string[] GetAnnouncementFilePathsFromDirectory(string dirPath) {
            string[] filePaths = Directory.GetFiles(dirPath, "*.txt");
            return filePaths;
        }

        private string GetAnnouncementDirectoryPath()
        {
            var config = Utility.Utility.GetConfig();
            var announcementDirpath = (string)config.val.announcementDirectorypath;
            return announcementDirpath;
        }
    }
}
